import { ImageBackground, StyleSheet, SafeAreaView } from 'react-native';
import StartGameScreen from './screens/StartGameScreen'
import { LinearGradient } from 'expo-linear-gradient'
import { useState } from 'react'
import GameScreen from './screens/GameScreen'
import Colors from './constants/Colors'

export default function App() {
  const [userNumber, setUserNumber] = useState()
  
  function pickedNumberHandler(pickerNumber){
    setUserNumber(pickerNumber)
  }
  let screen = <StartGameScreen onPickNumber={pickedNumberHandler} />
  if (userNumber) {
    screen = <GameScreen />
}
  
  return (
    <LinearGradient colors={[Colors.primary700, Colors.accent500]}
style={styles.rootScreen}>
      <ImageBackground
        source={require('./assets/iconkk.png')}
        resizeMode='cover'
        style = {styles.rootScreen}
        imageStyle={styles.backgroundImage}
      >
       <SafeAreaView style={styles.rootScreen}>{screen}</SafeAreaView> 
         
      </ImageBackground>
      
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  rootScreen: {
    flex: 1,
    // backgroundColor: '#ddb52f',
    },
    backgroundImage: {
      opacity: 0.15,
      },
});
